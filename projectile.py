import random
from collections import deque
from enum import Enum, auto
from itertools import islice
from typing import List

import pygame as pg
import pygame.gfxdraw

import settings as st
from effects import UnitDeathEffect, DamagePopup, OnHitEffect, ProjDeathEffect, ProjFlareEffect
from units import UnitGroup, Hero
from utils import get_color_mix, random_coords, load_sound


class Projectile(pg.sprite.Sprite):
    def __init__(self, clock: pg.time.Clock, speed: float = st.proj_speed):
        super().__init__()
        self.speed = speed
        self.show_range = st.show_proj_range_indicator
        self.damage = st.proj_damage
        self.radius = st.proj_radius
        self.bounce_count = 0

        self.state = ProjState.SEARCHING
        self.clock = clock
        self.delayed_for = 0
        self.trail = deque(maxlen=st.max_trail_length)
        self.fx = []
        self.gsm = 1.0

        self.image = pg.Surface(st.proj_size).convert()
        self.pos = pg.Vector2(random_coords())
        self.rect = self.image.get_rect(center=self.pos)

        self.target_unit = None
        self.target_pos = None
        self.move = None

        self.r_diff = st.proj_end_color[0] - st.proj_start_color[0]
        self.g_diff = st.proj_end_color[1] - st.proj_start_color[1]
        self.b_diff = st.proj_end_color[2] - st.proj_start_color[2]
        self.r_start = st.proj_start_color[0]
        self.g_start = st.proj_start_color[1]
        self.b_start = st.proj_start_color[2]

        self.hero_hit_sounds = [load_sound('Chain_Frost_target_hero1.mp3', 0.4),
                                load_sound('Chain_Frost_target_hero2.mp3', 0.4),
                                load_sound('Chain_Frost_target_hero3.mp3', 0.4)]
        self.creep_hit_sound = load_sound('Chain_Frost_target_creep.mp3', 0.35)

    def play_hit_sound(self):
        if isinstance(self.target_unit, Hero):
            random.choice(self.hero_hit_sounds).play()
        else:
            self.creep_hit_sound.play()

    def update(self, units: UnitGroup, gsm: float):
        self.gsm = gsm
        self.trail.appendleft(self.rect.center)
        if self.state == ProjState.DELAYED:
            self.delayed_for -= 1
            if self.delayed_for <= 0:
                self.state = ProjState.SEARCHING

        elif self.state == ProjState.SEARCHING:
            self.target_unit = units.select_new_target(self.target_unit, self.rect.center)
            if self.target_unit:
                self.state = ProjState.CHASING
                self.target_pos = pg.Vector2(self.target_unit.rect.center)
                self.target_unit.is_targeted = True
            else:
                self.add_proj_death_effect()
                self.state = ProjState.DONE

        elif self.state == ProjState.CHASING:
            if pg.sprite.collide_circle(self, self.target_unit):
                self.play_hit_sound()
                self.target_unit.is_targeted = False
                self.process_proj_hit()
                self.add_damage_popup()

                self.bounce_count += 1
                self.delayed_for = self.get_delay_duration_in_frames()

                if self.is_ran_out_of_bounces():
                    self.add_proj_death_effect()
                    self.state = ProjState.DONE
                else:
                    if self.delayed_for:
                        self.state = ProjState.DELAYED
                    else:
                        self.state = ProjState.SEARCHING

                self.add_hit_effect()
                self.add_flare_effect()
            else:
                # if target dies mid-chase (hit by other proj or otherwise), stop chasing
                if self.target_unit.hp <= 0:
                    self.state = ProjState.SEARCHING
                else:
                    self.target_unit.is_targeted = True
                    self.chase_target()

        self.update_effects()

        if self.state == ProjState.DONE and not self.fx:
            self.kill()

    def draw(self, screen: pg.Surface):
        self.draw_helpers(screen)
        if self.trail and self.state != ProjState.DONE:
            self.draw_trail(screen)
        for effect in self.fx:
            effect.draw(screen)

    def draw_helpers(self, scr: pg.Surface):
        if self.state != ProjState.DONE and self.show_range:
            pygame.gfxdraw.aacircle(scr, *self.rect.center,
                                    st.proj_range, st.proj_range_indicator_color)

    def draw_trail(self, screen: pg.Surface):
        trail_length = int(st.proj_visual_radius /
                           self.gsm /
                           st.proj_speed *
                           st.trail_magic_constant *
                           st.trail_custom_length)

        t_step = st.proj_visual_radius / trail_length / 1
        r_step = self.r_diff / trail_length
        g_step = self.g_diff / trail_length
        b_step = self.b_diff / trail_length

        for i, pos in enumerate(islice(self.trail, trail_length)):
            x, y = pos
            r = self.r_start + (i * r_step)
            g = self.g_start + (i * g_step)
            b = self.b_start + (i * b_step)
            rad = round(st.proj_visual_radius - (i * t_step))
            pygame.gfxdraw.aacircle(screen, x, y, rad, (r, g, b, st.proj_alpha))
            pygame.gfxdraw.filled_circle(screen, x, y, rad, (r, g, b, st.proj_alpha))

    def get_hit_damage(self) -> int:
        return self.damage + self.bounce_count * st.dmg_increase_per_bounce

    def process_proj_hit(self):
        self.target_unit.hp -= self.get_hit_damage()

        if self.target_unit.hp <= 0:
            x, y = self.target_unit.rect.center
            self.fx.append(UnitDeathEffect(x, y, self.target_unit.current_color, self.target_unit.radius))
            self.target_unit.kill()
            return

        cur_hp = self.target_unit.hp / self.target_unit.max_hp
        is_low_hp = self.target_unit.hp <= st.proj_damage
        self.target_unit.current_color = get_color_mix(self.target_unit.current_color,
                                                       st.hurt_unit_color,
                                                       cur_hp,
                                                       is_low_hp)
        if not self.target_unit.slowed_for:
            self.target_unit.cur_speed *= (1 - st.slow_effect)

        # refresh/apply the slow duration
        self.target_unit.slowed_for = round(self.clock.get_fps() * st.slow_duration)

    def is_ran_out_of_bounces(self) -> bool:
        if st.bounce_limit == 0:
            return False

        return self.bounce_count >= st.bounce_limit

    def get_delay_duration_in_frames(self) -> int:
        return round(self.clock.get_fps() * st.bounce_delay)

    def add_damage_popup(self):
        if self.clock.get_fps():
            alpha_step = st.dmg_popup_start_alpha / (self.clock.get_fps() * st.dmg_popup_duration)
        else:
            alpha_step = 1
        self.fx.append(DamagePopup(self.get_hit_damage(), self.target_unit.rect.center, alpha_step))

    def chase_target(self):
        self.target_pos = pg.Vector2(self.target_unit.rect.center)

        self.move = self.target_pos - self.pos
        move_length = self.move.length()

        if move_length < self.speed:
            self.pos = self.target_pos
        elif move_length != 0:
            self.move.normalize_ip()
            self.move = self.move * self.speed * self.gsm
            self.pos += self.move

        self.rect.center = self.pos

    def add_hit_effect(self):
        fps = self.clock.get_fps()
        self.fx.append(OnHitEffect(self.target_pos, fps or 100, self.move, self.target_unit.radius))

    def add_proj_death_effect(self):
        x, y = self.rect.center
        self.fx.append(ProjDeathEffect(x, y))

    def add_flare_effect(self):
        self.fx.append(ProjFlareEffect(self.target_pos))

    def update_effects(self):
        for f in self.fx:
            f.update()
            if f.is_need_to_remove():
                self.fx.remove(f)


class ProjState(Enum):
    SEARCHING = auto()
    CHASING = auto()
    DELAYED = auto()
    DONE = auto()


class ProjGroup(pg.sprite.Group):
    def add_projectiles(self, clock: pg.time.Clock, amount: int = st.proj_count):
        for i in range(amount):
            self.add(Projectile(clock))

    def sprites(self) -> List[Projectile]:
        return list(self.spritedict)

    def is_all_done(self) -> bool:
        return all(proj.state == ProjState.DONE for proj in self.sprites())

    def draw(self, surface: pg.Surface):
        for proj in self.sprites():
            proj.draw(surface)
        self.lostsprites = []

    def show_range_indicator(self):
        for proj in self.sprites():
            proj.show_range = True

    def dont_show_range_indicator(self):
        for proj in self.sprites():
            proj.show_range = False
