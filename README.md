# Chain Frost Simulation

Chain Frost is a [spell](https://dota2.gamepedia.com/Abilities)
in [Dota 2](https://en.wikipedia.org/wiki/Dota_2).

> Releases an orb of frost that bounces between nearby enemy units,
slowing and damaging each time it hits.

It has [pretty unique mechanics](https://dota2.gamepedia.com/Lich#Chain_Frost) and I thought
it could be fun to simulate its behavior.

How to use
----------
1. Make sure you have [Python 3.7+](https://www.python.org/downloads/)
2. Run `main.py` 🤡
- Press Space to pause the simulation at any moment
- Press R to restart it
- Press left ALT to show the range indicator(s)
- Press B to toggle the background
- Press ESC to quit

Features
--------
- Use original sound effects
- Arbitrary amount of units and projectiles
- Configurable parameters for units and projectiles: image size, max speed, etc
- Units can be heroes or creeps, they have different radius, hp and sound effect while hit
- Units can also be either static or moving
- Specify projectile range and speed
- The unit targeted by the projectile has a red target mark which is shown on top of everything
- Optionally show a circle indicating projectile range
- Optional [antialiasing](https://en.wikipedia.org/wiki/Spatial_anti-aliasing)
for circles representing the units
- Keep track of how many units projectiles managed to kill
and display it after projectiles stop
- Units have configurable hp
- Configurable number of bounces, unlimited bounces by default
- Units don't go offscreen, but rather get *reflected* off the screen edges and off of each other
- Show the "score", FPS and proj amount in the window caption
- Show damage popups
- Damage is increased after each bounce (added in patch 7.23)
- Unit's current hp is indicated by its color
- Optionally prioritize low hp units
- Optional delay after each bounce (0.1s default value, 0.2s in game)
- Temporarily slow units on each hit
- Find new target dynamically if the current one died
- Visual effect for hitting a unit (cold particles + flare effect)
- Visual effect for a unit dying (shrinking into the void)
- Visual effect for the projectile dying (expanding nova)

Videos
-----
- [In-game demonstration](https://youtu.be/dVK8n9JMVFQ?t=108)
- [Multiple simultaneous Chain Frosts in game](https://www.youtube.com/watch?v=SAO2yX4ENrM)
- [Early simulation video #1](https://www.youtube.com/watch?v=lZlakQneQ2c)
- [Early simulation video #2](https://www.youtube.com/watch?v=plmXhs8QlEs)
- [Current state of things](https://www.youtube.com/watch?v=lXPe5SciYmg)

Disclaimer
-----
This application is not affiliated, associated, authorized, endorsed by, or in any way officially connected to Valve Corporation.
The name Dota 2 and related names, marks, emblems and images are registered trademarks of their respective owners.