TODO
-------------------
- [x] A proper settings file
- [x] Custom HP for units
- [x] Color units differently based on their current hp
- [x] Improve the variance of unit speed/angle
- [x] Units get slowed by x% for x seconds
- [x] Projectile should look like a circle
- [x] Delay between bounces (0.2s or custom)
- [x] An optional limit for bounces
- [x] Game speed independent of FPS
- [x] Optional collision between units (pre-alpha)
- [x] Damage dealt popups
- [x] Projectile trail
- [x] Background image
- [x] Toggle ALT to show helpers
- [x] Explosion-like on-hit effect
- [x] Unit death effect
- [x] Proj death effect
- [x] Sound effects
- [ ] Add comments

Probably don't really need
--------------------------
- Total damage dealt
- Display HP as text

Other stuff
-----------
- Other visual effects
- Shadows for units and proj
- [good demo](https://www.youtube.com/watch?v=9PccXL5MGHA) to see effects