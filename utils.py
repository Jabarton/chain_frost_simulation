import math
import random
from pathlib import Path
from typing import Tuple

import numpy as np
import pygame as pg
import pygame.gfxdraw
from pygame.mixer import Sound
from scipy.stats import truncnorm

import settings as st


def random_coords(pad: int = 0) -> Tuple[int, int]:
    return random.randint(0 + pad, st.width - pad), random.randint(0 + pad, st.height - pad)


def get_distance(coords_x: Tuple[int, int], coords_y: Tuple[int, int]) -> float:
    x1, y1 = coords_x
    x2, y2 = coords_y
    return math.hypot(x2 - x1, y2 - y1)


def get_color_mix(c1: pg.Color, c2: pg.Color, cur_hp_percent: float, low_hp: bool) -> pg.Color:
    # if a unit dies in the next hit, make it look max hurt
    if low_hp:
        cur_hp_percent = 0

    *c1, _ = c1
    *c2, _ = c2
    r, g, b = [int(a * cur_hp_percent + b * (1 - cur_hp_percent)) for a, b in zip(c1, c2)]

    return pg.Color(r, g, b, st.unit_alpha)


def truncated_gaussian_speed() -> np.float64:
    lower = st.min_unit_speed
    upper = st.max_unit_speed
    mu = (upper - lower) / 2
    sigma = (upper - lower) / 4

    return truncnorm.rvs((lower - mu) / sigma,
                         (upper - mu) / sigma,
                         loc=mu,
                         scale=sigma)


def load_image(filename: str, no_conversion=False) -> pg.Surface:
    if not pg.display.get_init():
        pg.display.set_mode((st.width, st.height))

    image = str(Path(filename))
    if no_conversion:
        return pg.image.load(image)
    if filename.endswith('.jpg'):
        surface = pg.image.load(image).convert()
    else:
        surface = pg.image.load(image).convert_alpha()
    return surface


def draw_filled_circle(surf: pg.Surface, x: int, y: int, r: int, color: pg.Color):
    if st.aa_circles:
        pygame.gfxdraw.aacircle(surf, x, y, r, color)
    pygame.gfxdraw.filled_circle(surf, x, y, r, color)


def get_random_unit_color() -> pg.Color:
    r = random.randint(0, 10)
    g = random.randint(200, 255)
    b = random.randint(0, 100)
    return pg.Color((r, g, b))


def load_sound(name: str, volume: float = 0.5) -> Sound:
    if not pg.mixer.get_init():
        pg.mixer.init()

    path = str(Path(f'sound/{name}'))
    sound = Sound(path)
    sound.set_volume(volume)

    return sound


if __name__ == '__main__':
    boom = load_sound('Chain_Frost_cast.mp3')
    boom.play()
    input()
