import random
from itertools import combinations
from typing import List, Tuple, Optional

import numpy as np
import pygame as pg
import pygame.gfxdraw

import settings as st
from utils import (truncated_gaussian_speed, random_coords, load_image,
                   draw_filled_circle, get_distance, get_random_unit_color)


class Unit(pg.sprite.Sprite):
    def __init__(self, radius: int, max_hp: int):
        super().__init__()

        self.is_moving = st.moving_units
        self.radius = radius
        self.size = np.array((self.radius * 2 + 1, self.radius * 2 + 1))
        self.slow_image = pg.transform.smoothscale(SLOW_IMAGE, (self.size * st.slow_img_scale).astype(int))

        self.max_hp = max_hp
        self.hp = max_hp
        self.current_color = get_random_unit_color()

        self.slowed_for = 0  # amount of frames the unit is slowed for

        self.base_speed = truncated_gaussian_speed()
        self.cur_speed = self.base_speed
        self.pos = pg.Vector2(random_coords(pad=self.radius + 1))
        self.move = pg.Vector2(random.uniform(-1, 1), random.uniform(-1, 1))
        self.move.normalize_ip()

        self.image = pg.Surface(tuple(self.size), pg.SRCALPHA).convert_alpha()
        self.rect = self.image.get_rect(center=self.pos)

        self.is_targeted = False

    def update_unit(self, gsm: float):
        if self.is_moving:
            if self.rect.right >= st.width or self.rect.left <= 0:
                self.move.reflect_ip((1, 0))
            if self.rect.bottom >= st.height or self.rect.top <= 0:
                self.move.reflect_ip((0, 1))

            self.move = self.move.normalize() * self.cur_speed * gsm
            self.pos += self.move

            self.rect.center = self.pos

        if self.slowed_for:
            self.slowed_for -= 1
        elif self.cur_speed != self.base_speed:
            self.cur_speed = self.base_speed

    def draw_unit(self, surface: pg.Surface):
        surface.blit(self.image, self.rect)
        draw_filled_circle(self.image, self.radius, self.radius, self.radius, self.current_color)
        if self.slowed_for:
            slow_rect = self.slow_image.get_rect()
            slow_rect.center = self.rect.center
            surface.blit(self.slow_image, slow_rect)

    def is_in_range(self, pos: Tuple[int, int]) -> bool:
        return get_distance(pos, self.rect.center) <= st.proj_range


class Creep(Unit):
    def __init__(self):
        super().__init__(radius=st.creep_radius, max_hp=st.creep_hp)


class Hero(Unit):
    def __init__(self):
        super().__init__(radius=st.hero_radius, max_hp=st.hero_hp)


class UnitGroup(pg.sprite.Group):
    def add_units(self):
        for i in range(st.creep_count):
            self.add(Creep())

        for i in range(st.hero_count):
            self.add(Hero())

    def sprites(self) -> List[Unit]:
        return list(self.spritedict)

    def update(self, gsm: float):
        if st.unit_collision:
            for x, y in combinations(self.sprites(), 2):
                if pg.sprite.collide_circle(x, y):
                    x.move = x.pos - y.pos
                    y.move = y.pos - x.pos

        for s in self.sprites():
            s.update_unit(gsm)

    def draw(self, surface: pg.Surface):
        targets = []
        for spr in self.sprites():
            spr.draw_unit(surface)

            if spr.is_targeted:
                targets.append(spr)

        # not putting this in the first cycle so targets show up on top of all units
        for spr in targets:
            pygame.gfxdraw.rectangle(surface, spr.rect, pg.Color('red'))

        self.lostsprites = []

    def select_new_target(self, unit_last_hit: Unit, proj_center: Tuple[int, int]) -> Optional[Unit]:
        potential_targets = [x for x in self.sprites() if x.is_in_range(proj_center) and x != unit_last_hit]
        if not potential_targets:
            return

        if st.prioritize_low_hp_units:
            min_hp = min(x.hp for x in potential_targets)
            potential_targets = [x for x in potential_targets if x.hp == min_hp]

        return random.choice(potential_targets)


SLOW_IMAGE = load_image(st.slow_img_file)
