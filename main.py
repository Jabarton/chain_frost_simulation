import os
import random
import sys

import pygame as pg

import settings as st
from projectile import ProjGroup
from units import UnitGroup
from utils import load_image, load_sound


class App:
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pg.init()
    pg.display.set_mode((st.width, st.height))
    pg.display.set_icon(load_image(st.icon_file))
    random.seed()
    pg.display.set_caption(st.window_title)

    def __init__(self):
        self.screen = pg.display.get_surface()
        self.clock = pg.time.Clock()
        self.font = pg.font.SysFont(st.font_type, st.font_size, bold=True)
        self.bg = load_image(random.choice(st.bg_files))
        self.bg = pg.transform.smoothscale(self.bg, (st.width, st.height))
        self.bg_is_image = st.bg_is_image
        self.cast_sound = load_sound('Chain_Frost_cast.mp3')
        self.loop_sound = load_sound('Chain_Frost_loop.mp3', 0.25)

        self.paused = False
        self.units = None
        self.projectiles = None
        self.score = 0
        self.initial_unit_count = 0

        self.start_game()

    def start_game(self):
        self.paused = False
        self.units = UnitGroup()
        self.units.add_units()
        self.projectiles = ProjGroup()
        self.projectiles.add_projectiles(self.clock)
        self.score = 0
        self.initial_unit_count = len(self.units)
        self.cast_sound.play()
        self.loop_sound.play(loops=-1)

    @property
    def game_speed_modifier(self) -> float:
        if not self.clock.get_fps():
            return 1.0
        return st.game_speed / self.clock.get_fps()

    def event_loop(self):
        for event in pg.event.get():
            # Press ESC or ❌ to quit
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()

            if event.type == pg.KEYDOWN:
                # Press SPACE to pause the simulation
                if event.key == pg.K_SPACE:
                    self.paused = not self.paused
                    if not self.paused:
                        self.loop_sound.play(loops=-1)

                # Press B to toggle the background
                if event.key == pg.K_b:
                    self.bg_is_image = not self.bg_is_image

                # Press R to restart the game
                if event.key == pg.K_r:
                    self.start_game()

                # Hold left ALT to display proj helpers
                if event.key == pg.K_LALT:
                    self.projectiles.show_range_indicator()

            if event.type == pg.KEYUP:
                if event.key == pg.K_LALT:
                    self.projectiles.dont_show_range_indicator()

    def update_everything(self):
        if not self.paused:
            self.units.update(self.game_speed_modifier)
            self.projectiles.update(self.units, self.game_speed_modifier)
            self.score = self.initial_unit_count - len(self.units)

    def show_score(self):
        message = f'Chain Frost killed {self.score}/{self.initial_unit_count} units'
        result = self.font.render(message, True, st.font_color)
        w, h = result.get_size()
        self.screen.blit(result, (st.width / 2 - w / 2, st.height / 2 - h / 2))

    def draw_everything(self):
        if self.bg_is_image:
            self.screen.blit(self.bg, (0, 0))
        else:
            self.screen.fill(st.background_color)
        self.units.draw(self.screen)
        self.projectiles.draw(self.screen)
        if self.paused or self.projectiles.is_all_done():
            self.show_score()
            self.loop_sound.stop()

    def show_caption(self):
        pg.display.set_caption(f'{st.window_title} | Score: {self.score}/{self.initial_unit_count} '
                               f'| Projectiles: {len(self.projectiles)} '
                               f'| FPS: {round(self.clock.get_fps())}')

    def main_loop(self):
        while True:
            self.event_loop()
            self.update_everything()
            self.draw_everything()
            pg.display.flip()
            self.clock.tick_busy_loop(st.framerate)
            self.show_caption()


if __name__ == '__main__':
    app = App()
    app.main_loop()
