import random
from abc import abstractmethod
from collections import namedtuple
from dataclasses import dataclass
from typing import Tuple

import pygame as pg
import pygame.gfxdraw

import settings as st
from utils import draw_filled_circle, load_image

HitParticle = namedtuple('HitParticle', 'vec, color')


class BaseEffect:
    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def is_need_to_remove(self) -> bool:
        pass

    @abstractmethod
    def draw(self, screen: pg.Surface):
        pass


class OnHitEffect(BaseEffect):
    """Show this particle effect whenever the proj hits a unit.
    The effect is not the same as in Dota:
    particles are spreading in roughly the same direction as the proj was going
    """

    def __init__(self, pos: pg.Vector2, fps: float, proj_vec: pg.Vector2, unit_radius: int):
        self.pos = pos
        self.frames_left = round(fps * st.fx_effect_duration)
        self.rad_modifier = 1 + st.fx_effect_expansion_speed / fps
        self.fx_particle_len = unit_radius * st.fx_particle_len_modifier

        try:
            x, y = proj_vec.normalize()
        except AttributeError:
            x, y = 0, 0

        self.particles = []
        self.r = unit_radius

        n = random.randint(st.fx_min_particles, st.fx_max_particles)
        for p in range(n):
            hspeed = random.gauss(-x, st.fx_particle_spread)
            vspeed = random.gauss(-y, st.fx_particle_spread)

            # Colors are hardcoded for now
            mod = random.uniform(1, min(255 / 80, 255 / 150))
            r = int(80 * mod)
            g = int(150 * mod)
            b = 255
            self.particles.append(HitParticle(pg.Vector2(hspeed, vspeed), (r, g, b)))

    def update(self):
        self.r *= self.rad_modifier
        self.frames_left -= 1

    def is_need_to_remove(self) -> bool:
        return not self.frames_left

    def draw(self, screen: pg.Surface):
        for p in self.particles:
            start = self.pos - p.vec * self.r - p.vec * self.fx_particle_len
            end = self.pos - p.vec * self.r
            pg.draw.aaline(screen, p.color, start, end)


@dataclass
class UnitDeathEffect(BaseEffect):
    """A unit 'collapses' whenever it dies"""
    x: int
    y: int
    color: pg.Color
    r: int

    def update(self):
        self.r = int((self.r ** st.fx_unit_shrink_ratio))

    def is_need_to_remove(self) -> bool:
        return self.r < 2

    def draw(self, screen: pg.Surface):
        draw_filled_circle(screen, self.x, self.y, self.r, self.color)


@dataclass
class ProjDeathEffect(BaseEffect):
    """Throw a nova on proj death"""
    x: int
    y: int
    color: pg.Color = st.fx_proj_nova_color
    r: int = 2
    half_width: int = st.fx_proj_nova_width // 2
    alpha_step: float = (st.fx_proj_nova_max_alpha - st.fx_proj_nova_min_alpha) / half_width

    def update(self):
        self.r = int((self.r ** st.fx_proj_expand_ratio) + 1)

    def is_need_to_remove(self) -> bool:
        return self.r > st.width

    def draw(self, screen: pg.Surface):
        alpha = st.fx_proj_nova_min_alpha
        for i in range(self.half_width):
            self.color.a = int(alpha)
            pygame.gfxdraw.aacircle(screen, self.x, self.y, self.r + i, self.color)
            alpha += self.alpha_step

        next_r = self.r + self.half_width
        for i in range(self.half_width):
            self.color.a = int(alpha)
            pygame.gfxdraw.aacircle(screen, self.x, self.y, next_r + i, self.color)
            alpha -= self.alpha_step


@dataclass
class ProjFlareEffect(BaseEffect):
    """Quick flare after every bounce"""
    pos: pg.Vector2
    n: int = st.fx_flare_images
    index: int = 0

    def update(self):
        self.index += 1

    def is_need_to_remove(self) -> bool:
        return self.index + 1 > self.n

    def draw(self, screen: pg.Surface):
        img = PROJ_FLARE.images[self.index]
        rect = img.get_rect()
        rect.center = self.pos
        screen.blit(img, rect)


class ProjFlareImages:
    """The object that makes and holds the flare images"""

    def __init__(self, amount: int):
        self.base_image = load_image('img/flare_new.png')
        self.images = []
        size_step = 10
        for i in range(1, amount + 1):
            img = pg.transform.smoothscale(self.base_image, (i * size_step, i * size_step))
            self.images.append(img)


@dataclass
class DamagePopup(BaseEffect):
    def __init__(self, damage: int, pos: Tuple[int, int], alpha_step: float, alpha: int = st.dmg_popup_start_alpha):
        self.damage = damage
        self.pos = pos
        self.alpha_step = alpha_step
        self.alpha = alpha

        self.dmg_popup_surface = DMG_POPUP_FONT.render(f'{damage}', True, st.dmg_popup_color)
        self.dmg_popup_alpha_surface = pg.Surface(self.dmg_popup_surface.get_size(), pg.SRCALPHA)

    def update(self):
        self.alpha = max(self.alpha - self.alpha_step, 0)

    def is_need_to_remove(self) -> bool:
        return self.alpha <= 0

    def draw(self, screen: pg.Surface):
        popup_copy = self.dmg_popup_surface.copy()
        self.dmg_popup_alpha_surface.fill((255, 255, 255, self.alpha))
        popup_copy.blit(self.dmg_popup_alpha_surface, (0, 0), special_flags=pg.BLEND_RGBA_MULT)

        screen.blit(popup_copy, self.pos)


PROJ_FLARE = ProjFlareImages(st.fx_flare_images)
pg.font.init()
DMG_POPUP_FONT = pg.font.SysFont(st.font_type, st.font_size, bold=True)
