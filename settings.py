import logging

import numpy as np
import pygame as pg

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')
logger = logging.getLogger()

# -------------------------------WINDOW SETTINGS--------------------------------
window_title = 'Chain Frost Simulation'
width = 1280
height = 720
window_center = (width / 2, height / 2)

# 0 - uncapped framerate
framerate = 167

# -----------------------------SIMULATION SETTINGS------------------------------
performance_test = False

game_speed = 110
proj_count = 2

creep_count = 20
creep_radius = 40
creep_hp = 600

hero_count = 5
hero_radius = 70
hero_hp = 2000

min_unit_speed = 0.1
max_unit_speed = 3.2

proj_speed = 3.5
proj_range = 400
proj_damage = 400
# radius that is used for collision
proj_radius = 0
proj_visual_radius = 28
proj_size = np.array((proj_visual_radius * 2 + 1, proj_visual_radius * 2 + 1))

# default in-game values for slow: 65% for 2.5s
slow_effect = 0.65
slow_duration = 1

# 0 - unlimited bounces (Aghanim's Scepter upgrade)
# default in-game value is 10 bounces
# and each bounce has a 0.2s delay
bounce_limit = 0
bounce_delay = 0.1
dmg_increase_per_bounce = 25

prioritize_low_hp_units = False

# --------------------------------VIDEO SETTINGS--------------------------------
bg_is_image = True

show_proj_range_indicator = False

slow_img_scale = 0.7
unit_alpha = 255

# antialiasing for unit circles, affects performance
aa_circles = True

trail_custom_length = 1
max_trail_length = 300
trail_magic_constant = 5.3

# ---------------------------------FX SETTINGS----------------------------------
# How long the effect persists. Value is in seconds
fx_effect_duration = 0.15

# How long each particle is
fx_particle_len_modifier = 0.277

# How fast the effect expands, covering more ground
fx_effect_expansion_speed = 8.3 / 1.5

fx_min_particles = 25
fx_max_particles = 45
fx_particle_spread = 0.6

fx_unit_shrink_ratio = 0.99
fx_proj_expand_ratio = 1.015
fx_proj_nova_width = 60
fx_proj_nova_color = pg.Color((80, 150, 255, 255))
fx_proj_nova_min_alpha = 0
fx_proj_nova_max_alpha = 255

fx_flare_images = 30

# --------------------------------FONT SETTINGS---------------------------------
font_type = 'consolas'
font_size = 20
font_color = pg.Color('yellow')

dmg_popup_size = 10
dmg_popup_color = pg.Color('red')
dmg_popup_duration = 1.5
dmg_popup_start_alpha = 255

# --------------------------------COLOR SETTINGS--------------------------------
background_color = pg.Color('gray10')

unit_color = pg.Color('green3')
unit_color.a = unit_alpha
hurt_unit_color = pg.Color('red')

proj_path_color = (80, 150, 255)
proj_range_indicator_color = pg.Color('white')

proj_start_color = (80, 150, 255)
proj_end_color = pg.Color('white')
proj_alpha = 255

# --------------------------------MISC SETTINGS---------------------------------
moving_units = True
unit_collision = True

bg_files = ('img/dota_background.jpg', 'img/dota_background2.jpg')
icon_file = 'img/chain_frost.png'
slow_img_file = 'img/freezing-face_facebook.png'

# -------------------------PERFORMANCE TEST OVERRIDES---------------------------
if performance_test:
    moving_units = True
    framerate = 0
    game_speed = 110
    creep_count = 2000
    creep_radius = 19
    creep_hp = 1000
    hero_count = 0
    proj_range = 200
    unit_alpha = 255
    max_unit_speed = 2
    aa_circles = False
    proj_visual_radius = 13

    proj_size = np.array((proj_visual_radius * 2 + 1, proj_visual_radius * 2 + 1))
    unit_color.a = unit_alpha

    prioritize_low_hp_units = False
    unit_collision = False
